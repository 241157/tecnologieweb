# Energym

Questo è il file README che contiene tutte le istruzioni per provare
il nostro progetto sul tuo PC.

Per farlo devi innanzitutto creare una cartella in locale e clonare al suo interno questa repository:
> git clone https://gitlab.com/241157/tecnologieweb.git


Il progetto è stato sviluppato utilizzando python 3.8. Per installarlo segui le
istruzioni contenute nei link:

- installazione su sistemi linux → https://wiki.python.org/moin/BeginnersGuide/Download

- installazione su MacOs → https://installpython3.com/mac/

- installazione su WIndows → download .exe from https://www.python.org/downloads/

## Configurazione

Posizionati nella cartella del progetto e assicurati di avere installato **pipenv**  
> python3 -m pip --version

oppure installalo
> pip3 install pipenv


Installa **Django** nel virtual environment
> pipenv install django

e attivalo
> pipenv shell


Per installare tutti i packages necessari all'applicazione, assicurati di essere
nella directory contenente il file requirements.txt e digita:

> pip install -r requirements.txt

## Esecuzione

Per lanciare il web server che ospiterà l'applicazione, esegui:
> python manage.py runserver


## Tests

Per eseguire i test:
> python manage.py test 