

def my_context_processor(request):
    from gestione_palestra.models import Corso
    corsi = Corso.objects.all()
    return {'Corsi': corsi}
