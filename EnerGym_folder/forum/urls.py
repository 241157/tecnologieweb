from django.contrib.staticfiles.storage import staticfiles_storage
from django.urls import path
from django.urls import re_path
from django.views.generic import RedirectView
from . import views

app_name = 'forum'

urlpatterns = [
    path('forum', views.forum_homepage, name='forum_home'),
    path('forum/topic/<int:id>', views.topic, name='topic_info'),
    path('forum/topic/add_comment', views.crea_commento, name='crea_commento'),
    path('forum/topic/update_commento/<int:pk>', views.update_commento, name='update_commento'),
    path('forum/topic/delete_commento/<int:pk>', views.delete_commento, name='delete_commento'),
    path('forum/topic/add_topic', views.add_topic, name='add_topic'),
    path('forum/topic/delete_topic/<int:pk>', views.delete_topic, name='delete_topic'),
    path('favicon.ico', RedirectView.as_view(url=staticfiles_storage.url('img/Energym.ico'))),
    re_path(r'^$', RedirectView.as_view(url='homepage', permanent=False), name='index'),
]
