from django import forms

from forum import models
from forum.models import Commento, Topic_lezione


class CommentoForm(forms.ModelForm):

    class Meta:
        model = Commento
        fields = ('testo',)
        widgets = {
            'topic': forms.HiddenInput(),
            'gymuser_lezione': forms.HiddenInput(),
        }


class TopicForm(forms.ModelForm):

    class Meta:
        model = Topic_lezione
        fields = ('titolo', 'descrizione')
        widgets = {
            'gymuser_topic': forms.HiddenInput(),
        }
