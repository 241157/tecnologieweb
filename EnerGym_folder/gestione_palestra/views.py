from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.views.generic import CreateView, DetailView, UpdateView
from django.shortcuts import render, redirect
from gestione_palestra.forms import SignUpForm, PasswordChange, ModifyUserForm
from gestione_palestra.models import Corso, Notizia, Gallery, GymUser
from lezioni.models import Prenotazione


def homepage(request):
    notizie = Notizia.objects.all().order_by('-data_creazione')[:6]
    notizie_consigliate = None

    if request.user.is_authenticated:
        user = request.user
        lezioni_utente = Prenotazione.objects.filter(user_id=user.id).values_list('lezione__insegnantecorso__corso__nome', flat=True).distinct()

        data = set()

        for nome_corso in lezioni_utente:
            data.add((Notizia.objects.filter(titolo__contains=nome_corso).order_by('data_creazione').values('id')[:1]))

        notizie_consigliate = Notizia.objects.filter(pk__in=data).order_by('-data_creazione')[:3]

    return render(request, 'gestione_palestra/homepage.html', {'notizie': notizie, 'notizie_consigliate': notizie_consigliate})


class SignUp(CreateView):
    model = GymUser
    form_class = SignUpForm
    template_name = 'registration/registrazione.html'
    success_url = reverse_lazy('gestione_palestra:homepage')

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = 'cliente'
        return super().get_context_data(**kwargs)


def istruttori(request):
    ins = GymUser.objects.filter(is_insegnante=True)
    return render(request, 'gestione_palestra/insegnanti.html', {'insegnanti': ins})


def istruttore_info(request, id):
    ins = GymUser.objects.get(id=id)
    corsi = ins.return_corsi
    return render(request, 'gestione_palestra/insegnante_info.html', {'insegnante': ins, 'corsi':corsi})


def corsi(request):
    return render(request, 'gestione_palestra/corsi.html')


def corso_info(request, id):
    corso = Corso.objects.get(id=id)
    return render(request, 'gestione_palestra/corso_info.html', {'corso': corso})


def check_username_is_unique(request):
    user = None
    username = request.GET.get('username', False)
    if username:
        user = User.objects.filter(username=username)
    return JsonResponse({'is_not_unique': True if user else False})


def contatti(request):
    return render(request, 'gestione_palestra/contatti.html')


def gallery(request):
    photos = Gallery.objects.all()
    return render(request, 'gestione_palestra/gallery.html', {'photos': photos})


def notizia_info(request, id):
    news = Notizia.objects.get(id=id)
    return render(request, 'gestione_palestra/notizia.html', {'news': news})


class ShowProfile(LoginRequiredMixin, DetailView):
    model = GymUser
    template_name = 'gestione_palestra/profile.html'


class ModifyUser(LoginRequiredMixin, UpdateView):
    model = GymUser
    template_name = 'gestione_palestra/modificauser.html'
    form_class = ModifyUserForm
    success_url = reverse_lazy('gestione_palestra:profile')

    def get_success_url(self):
        user_id = self.kwargs['pk']
        return reverse_lazy('gestione_palestra:profile', kwargs={'pk': user_id})


@login_required()
def PasswordChangeView(request):
    if request.method == 'POST':
        form = PasswordChange(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            return redirect('gestione_palestra:profile', pk=request.user.id)
    else:
        form = PasswordChange(request.user)
    return render(request, 'gestione_palestra/password_change.html', {'form': form})


def notizie(request):
    notizie = Notizia.objects.all().order_by('-data_creazione')
    return render(request, 'gestione_palestra/notizie.html', {'notizie': notizie})
