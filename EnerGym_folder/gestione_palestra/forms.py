from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm
from django import forms
from gestione_palestra.models import GymUser, InsegnanteCorso


class SignUpForm(UserCreationForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['first_name'].widget.attrs['placeholder'] = 'Nome'
        self.fields['last_name'].widget.attrs['placeholder'] = 'Cognome'
        self.fields['username'].widget.attrs['placeholder'] = 'Username'
        self.fields['email'].widget.attrs['placeholder'] = 'esempio@esempio.com'
        self.fields['password1'].widget.attrs['placeholder'] = 'Immetti una password'
        self.fields['password2'].widget.attrs['placeholder'] = 'Ripeti la password'
        self.helper = FormHelper()
        self.helper.add_input(
            Submit('submit', 'Registrati', css_class='btn btn-success')
        )

    class Meta:
        model = GymUser
        fields = (
            'first_name',
            'last_name',
            'username',
            'email',
            'password1',
            'password2',
            'profile_img'
        )

    def clean_first_name(self):
        return self.cleaned_data['first_name'].capitalize()

    def clean_last_name(self):
        return self.cleaned_data['last_name'].capitalize()


class ModifyUserForm(forms.ModelForm):
    helper = FormHelper()
    helper.form_id = 'modify_user_form'
    helper.form_method = 'POST'
    helper.add_input(Submit('submit', 'Salva'))

    class Meta:
        model = GymUser
        fields = ('first_name', 'last_name', 'username', 'email', 'descrizione_per_insegnante', 'profile_img')

    def clean_first_name(self):
        return self.cleaned_data['first_name'].capitalize()

    def clean_last_name(self):
        return self.cleaned_data['last_name'].capitalize()


class PasswordChange(PasswordChangeForm):
    helper = FormHelper()
    helper.form_id = 'add_test_form'
    helper.form_method = 'POST'
    helper.add_input(Submit('submit', 'Modifica password'))


class InsegnanteCorsoForm(forms.ModelForm):
    class Meta:
        model = InsegnanteCorso
        fields = ('corso', 'insegnante')

    def __init__(self, *args, **kwargs):
        super(InsegnanteCorsoForm, self).__init__(*args, **kwargs)
        insegnanti = GymUser.objects.filter(is_insegnante=True)
        self.fields['insegnante'].queryset = insegnanti
