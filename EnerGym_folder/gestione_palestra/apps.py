from django.apps import AppConfig


class GestionePalestraConfig(AppConfig):
    name = 'gestione_palestra'
