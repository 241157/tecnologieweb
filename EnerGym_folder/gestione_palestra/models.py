from django.contrib.auth.models import AbstractUser, User
from django.db import models
from django.template.defaultfilters import register


class GymUser(AbstractUser):
    profile_img = models.ImageField(upload_to='users_profile_img', default="profile_img.png")
    is_insegnante = models.BooleanField(default=False)
    descrizione_per_insegnante = models.TextField(max_length=400, blank=True)

    class Meta:
        verbose_name_plural = 'GymUser'

    def __str__(self):
        return self.first_name + ' ' + self.last_name

    @register.filter
    def return_corsi(self):
        id_corsi_insegnante = InsegnanteCorso.objects.filter(insegnante_id=self.id).values_list('corso_id', flat=True)
        corsi = Corso.objects.filter(id__in=id_corsi_insegnante)

        return corsi



class Corso(models.Model):
    nome = models.CharField(max_length=40)
    descrizione = models.TextField(max_length=400)

    class Meta:
        verbose_name_plural = 'Corsi'

    def __str__(self):
        return self.nome


class InsegnanteCorso(models.Model):
    corso = models.ForeignKey(Corso, on_delete=models.PROTECT, related_name='corso')
    insegnante = models.ForeignKey(GymUser, on_delete=models.PROTECT, related_name='insegnante')

    class Meta:
        verbose_name_plural = 'Insegnante-Corso'
        unique_together = ("corso", "insegnante")

    def __str__(self):
        return 'Corso: ' + self.corso.nome + ' - Insegnante: ' + self.insegnante.first_name + ' ' + self.insegnante.last_name

    def return_corso(self):
        return self.corso.nome


class Notizia(models.Model):
    titolo = models.CharField(max_length=40)
    descrizione = models.TextField(max_length=999)
    data_creazione = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = 'Notizie'

    def __str__(self):
        return self.titolo


class Gallery(models.Model):
    photo_path = models.ImageField(upload_to='gallery')

    def __str__(self):
        immagine = str(self.photo_path)
        return immagine

    class Meta:
        verbose_name_plural = 'ImmaginiGallery'
