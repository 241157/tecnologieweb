from django import forms
from django.utils import timezone

from lezioni.models import Lezione

from gestione_palestra.models import InsegnanteCorso


class LezioneForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super().__init__(*args, **kwargs)
        queryset = InsegnanteCorso.objects.filter(insegnante_id=user)
        self.fields['insegnantecorso'] = forms.ModelChoiceField(queryset=queryset)
        self.fields['insegnantecorso'].label_from_instance = lambda obj: "%s" % obj.return_corso()
        self.fields['data'].widget.attrs['placeholder'] = 'yyyy-mm-dd hh:mm'

    class Meta:
        model = Lezione
        fields = ('insegnantecorso', 'descrizione', 'posti_disponibili_tot', 'data')

    def clean_data(self):
        data = self.cleaned_data.get('data')
        if data < timezone.now():
            raise forms.ValidationError("Inserire una data valida")
        return data

    def clean_posti_disponibili_tot(self):
        posti_disponibili_new = self.cleaned_data.get('posti_disponibili_tot')

        if posti_disponibili_new <= 0:
            raise forms.ValidationError("Inserire un valore maggiore di 0")

        if self.instance.posti_disponibili_tot is not None:
            lezione = Lezione.objects.get(id=self.instance.id)
            if lezione.return_posti_occupati() >= posti_disponibili_new:
                raise forms.ValidationError("Ci sono troppi utenti gà iscritti a questa lezione")

        return posti_disponibili_new
