from django.apps import AppConfig


class LezioniConfig(AppConfig):
    name = 'lezioni'
