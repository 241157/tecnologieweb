from django.urls import re_path
from django.urls import path
from django.views.generic import RedirectView
from . import views

app_name = 'lezioni'

urlpatterns = [
    path('lezioni', views.lezioni, name='lezioni'),
    path('lezioni/le_mie_lezioni/<int:id>', views.gestione_lezioni, name='gestione_lezioni'),
    path('lezioni/crea', views.crea_lezione, name='crea_lezione'),
    path('lezioni/update/<int:pk>', views.update_lezione, name='update_lezione'),
    path('lezioni/delete/<int:pk>', views.delete_lezione, name='delete_lezione'),
    path('lezioni/prenotate/<int:id>', views.prenotazioni, name='prenotazioni'),
    path('lezioni/prenota', views.Prenota.as_view(), name='prenota'),
    path('lezioni/prenota/delete', views.DeletePrenotazione.as_view(), name='delete_prenotazione'),
    re_path(r'^$', RedirectView.as_view(url='homepage', permanent=False), name='index'),
]