from django.contrib import admin
from lezioni.models import Lezione, Prenotazione

admin.site.register(Lezione)
admin.site.register(Prenotazione)
