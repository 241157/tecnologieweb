from django.contrib.auth.decorators import user_passes_test, login_required
from EnerGym import settings


def insegnante_only(view_func):
    user_is_insegnante = user_passes_test(
        lambda user: user.is_insegnante,
        login_url=settings.LOGIN_URL
    )
    return login_required(user_is_insegnante(view_func))
