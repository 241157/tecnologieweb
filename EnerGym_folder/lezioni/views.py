from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404
from django.template.loader import render_to_string
from django.views.generic import DeleteView, TemplateView

from gestione_palestra.models import GymUser, Corso, InsegnanteCorso
from lezioni.decorators import insegnante_only
from lezioni.forms import LezioneForm
from lezioni.models import Lezione, Prenotazione


@login_required
def lezioni(request):
    prenot = Prenotazione.objects.filter(user_id=request.user.id).values_list('lezione_id', flat=True)
    lezioni_prenotate = Lezione.objects.filter(id__in=prenot)
    lezioni = Lezione.objects.all().order_by('-data')

    query = request.GET.get('q')
    if query:
        lezioni = lezioni.filter(
            Q(insegnantecorso__corso__nome__icontains=query)
        ).distinct()

    return render(request, 'lezioni/lezioni.html', {'lezioni': lezioni,
                                                    'lezioni_prenotate': lezioni_prenotate}
                  )


@insegnante_only
@login_required
def gestione_lezioni(request, id):
    id_corsi_insegnante = InsegnanteCorso.objects.filter(insegnante_id=id).values_list('corso_id', flat=True)
    id_insegnante_corso = InsegnanteCorso.objects.filter(insegnante_id=id).values_list('id', flat=True)
    corsi_ins = Corso.objects.filter(id__in=id_corsi_insegnante)
    lezioni = Lezione.objects.filter(insegnantecorso_id__in=id_insegnante_corso)

    return render(request, 'lezioni/gestione_lezioni.html', {'lezioni': lezioni, 'corsi_ins': corsi_ins})


@insegnante_only
@login_required
def save_lezione(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            id_insegnante_corso = InsegnanteCorso.objects.filter(insegnante_id=request.user.id).values_list('id',
                                                                                                            flat=True)
            lezioni = Lezione.objects.filter(insegnantecorso_id__in=id_insegnante_corso)
            data['html_lezioni_list'] = render_to_string('lezioni/lista_lezioni.html', {'lezioni': lezioni})
        else:
            data['form_is_valid'] = False

    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@insegnante_only
@login_required
def crea_lezione(request):
    if request.method == 'POST':
        form = LezioneForm(request.POST, user=request.user.id)
    else:
        form = LezioneForm(user=request.user.id)
    return save_lezione(request, form, 'lezioni/crea_lezione.html')


@insegnante_only
@login_required
def update_lezione(request, pk):
    lezione = get_object_or_404(Lezione, pk=pk)
    if request.method == 'POST':
        form = LezioneForm(request.POST, instance=lezione, user=request.user.id)
    else:
        form = LezioneForm(instance=lezione, user=request.user.id)
    return save_lezione(request, form, 'lezioni/update_lezione.html')


@insegnante_only
@login_required
def delete_lezione(request, pk):
    lezione = get_object_or_404(Lezione, pk=pk)
    data = dict()
    if request.method == 'POST':
        lezione.delete()
        data['form_is_valid'] = True
        id_insegnante_corso = InsegnanteCorso.objects.filter(insegnante_id=request.user.id).values_list('id', flat=True)
        lezioni = Lezione.objects.filter(insegnantecorso_id__in=id_insegnante_corso)
        data['html_lezioni_list'] = render_to_string('lezioni/lista_lezioni.html', {'lezioni': lezioni})
    else:
        context = {'lezione': lezione}
        data['html_form'] = render_to_string('lezioni/delete_lezione.html', context, request=request)

    return JsonResponse(data)


@login_required
def prenotazioni(request, id):
    lezioni = Prenotazione.objects.filter(user_id=id)
    return render(request, 'lezioni/prenotazioni.html', {'lezioni': lezioni})


class Prenota(LoginRequiredMixin, TemplateView):
    def get(self, request):
        user_id = request.GET.get('user', None)
        lez_id = request.GET.get('lezione', None)
        user = GymUser.objects.get(id=user_id)
        lezione = Lezione.objects.get(id=lez_id)
        prenotazione = Prenotazione(user=user, lezione=lezione)
        prenotazione.save()
        data = {
            'added': True
        }
        return JsonResponse(data)


class DeletePrenotazione(LoginRequiredMixin, DeleteView):
    def get(self, request):
        id_prenot = request.GET.get('id', None)
        Prenotazione.objects.get(id=id_prenot).delete()
        data = {
            'deleted': True
        }
        return JsonResponse(data)
